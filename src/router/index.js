import Vue from 'vue';
import Router from 'vue-router';
import scene1 from '@/components/scene1';
import test from '@/components/test';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'scene1',
      component: scene1,
    },
    {
      path: '/test',
      name: 'test',
      component: test,
    },
  ],
});
