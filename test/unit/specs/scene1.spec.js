import Vue from 'vue';
import { mount } from 'vue-test-utils';
import scene1 from '@/components/scene1';

describe('scene1.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(scene1);
  });

  it('should render the correct contents', () => {
    see('.animcontainer');
    see('#typewriter');
  });

  function see(selector, text) {
    if(text) {
      let wrap = wrapper.find(selector);
      expect(wrap.text()).toContain(text);
    } else {
      expect(wrapper.contains(selector)).toEqual(true);
    }
  }
});
