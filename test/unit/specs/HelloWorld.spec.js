import { mount } from 'vue-test-utils';
import HelloWorld from '@/components/HelloWorld';

describe('HelloWorld.vue', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(HelloWorld);
  });

  test('should render correct contents', () => {
    expect(wrapper.html()).toContain('Welcome to Your Vue.js App');
  });
});
